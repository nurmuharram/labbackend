package request

import (
	"idsbackend/usecase/labs"
)

type Labs struct {
	LabName     string  `json:"lab_name"`
	Price       float32 `json:"price"`
	Description string  `json:"description"`
}

func (req *Labs) ToDomain() *labs.Domain {
	return &labs.Domain{
		LabName:     req.LabName,
		Price:       req.Price,
		Description: req.Description,
	}
}
