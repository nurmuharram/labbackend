package labs

import (
	"idsbackend/controllers/labs/request"
	"idsbackend/controllers/labs/response"
	"idsbackend/usecase/labs"
	"net/http"
	"strconv"

	controller "idsbackend/controllers"

	echo "github.com/labstack/echo/v4"
)

type LabController struct {
	labUsecase labs.Usecase
}

func NewLabController(cu labs.Usecase) *LabController {
	return &LabController{
		labUsecase: cu,
	}
}

func (ctrl *LabController) SelectAll(c echo.Context) error {
	ctx := c.Request().Context()

	resp, err := ctrl.labUsecase.GetAll(ctx)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Lab{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	return controller.NewSuccessResponse(c, responseController)
}

func (ctrl *LabController) FindAll(c echo.Context) error {
	ctx := c.Request().Context()
	page, _ := strconv.Atoi(c.QueryParam("page"))
	limit, _ := strconv.Atoi(c.QueryParam("limit"))

	if page <= 0 {
		page = 1
	}
	if limit <= 0 || limit > 50 {
		limit = 10
	}

	resp, total, err := ctrl.labUsecase.FindAll(ctx, page, limit)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Lab{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	pagination := controller.PaginationRes(page, total, limit)
	return controller.NewSuccessResponsePagination(c, responseController, pagination)
}

func (ctrl *LabController) FindById(c echo.Context) error {
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	lab, err := ctrl.labUsecase.GetByID(ctx, id)

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(lab))
}

func (ctrl *LabController) Store(c echo.Context) error {
	ctx := c.Request().Context()

	req := request.Labs{}
	if err := c.Bind(&req); err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	resp, err := ctrl.labUsecase.Store(ctx, req.ToDomain())
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(resp))
}
