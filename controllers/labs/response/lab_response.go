package response

import (
	"idsbackend/usecase/labs"
	"time"

	"gorm.io/gorm"
)

type Lab struct {
	ID          int            `json:"id"`
	LabName     string         `json:"lab_name"`
	Price       float32        `json:"price"`
	Description string         `json:"description"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
}

func FromDomain(domain labs.Domain) Lab {
	return Lab{
		ID:          domain.ID,
		LabName:     domain.LabName,
		Price:       domain.Price,
		Description: domain.Description,
		CreatedAt:   domain.CreatedAt,
		UpdatedAt:   domain.UpdatedAt,
		DeletedAt:   domain.DeletedAt,
	}
}
