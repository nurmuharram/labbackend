package request

import (
	"idsbackend/usecase/bookings"
)

type Bookings struct {
	LabID int    `json:"lab_id"`
	Name  string `json:"name"`
	Date  string `json:"date"`
}

func (req *Bookings) ToDomain() *bookings.Domain {
	return &bookings.Domain{
		LabID: req.LabID,
		Name:  req.Name,
		Date:  req.Date,
	}
}
