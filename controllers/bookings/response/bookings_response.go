package response

import (
	"idsbackend/usecase/bookings"
	"time"

	"gorm.io/gorm"
)

type Bookings struct {
	ID          int            `json:"id"`
	LabID       int            `json:"lab_id"`
	LabName     string         `json:"labname"`
	Description string         `json:"description"`
	Name        string         `json:"name"`
	Date        string         `json:"date"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
}

func FromDomain(domain bookings.Domain) Bookings {
	return Bookings{
		ID:          domain.ID,
		LabID:       domain.LabID,
		LabName:     domain.LabName,
		Description: domain.Description,
		Name:        domain.Name,
		Date:        domain.Date,
		CreatedAt:   domain.CreatedAt,
		UpdatedAt:   domain.UpdatedAt,
		DeletedAt:   domain.DeletedAt,
	}
}
