package bookings

import (
	"idsbackend/controllers/bookings/request"
	"idsbackend/controllers/bookings/response"
	"idsbackend/usecase/bookings"
	"net/http"
	"strconv"

	controller "idsbackend/controllers"

	echo "github.com/labstack/echo/v4"
)

type BookingsController struct {
	BookingsUC bookings.Usecase
}

func NewBookingsController(uc bookings.Usecase) *BookingsController {
	return &BookingsController{
		BookingsUC: uc,
	}
}

func (ctrl *BookingsController) GetById(c echo.Context) error {
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	favourite, err := ctrl.BookingsUC.GetByID(ctx, id)

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(favourite))

}

// func (ctrl *BookingsController) Store(c echo.Context) error {
// 	fmt.Println("Test")
// 	ctx := c.Request().Context()

// 	req := request.Bookings{}
// 	if err := c.Bind(&req); err != nil {
// 		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
// 	}

// 	domainReq := req.ToDomain()
// 	fmt.Println("Test")
// 	resp, err := ctrl.BookingsUC.Store(ctx, domainReq)

// 	if err != nil {
// 		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
// 	}

// 	return controller.NewSuccessResponse(c, response.FromDomain(resp))
// }

func (ctrl *BookingsController) Store(c echo.Context) error {
	ctx := c.Request().Context()

	req := request.Bookings{}
	if err := c.Bind(&req); err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	resp, err := ctrl.BookingsUC.Store(ctx, req.ToDomain())
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(resp))
}
