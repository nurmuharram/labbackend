### Clone repo

``` bash
# clone the repo
$ git clone git@gitlab.com:nurmuharram/labbackend.git

# go into app's directory
$ cd labbackend

# checkout stable version
$ git checkout main
```

#### Run Backend
```
# go into app to run backend
$ cd app

# create database using mysql with name ids



# Start
$ go run main.go
```