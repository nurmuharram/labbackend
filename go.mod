module idsbackend

go 1.15

require (
	github.com/labstack/echo/v4 v4.7.2
	github.com/spf13/viper v1.10.1
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.4
)
