package bookings

import (
	"context"
	"fmt"
	usecase "idsbackend/usecase"
	"time"
)

type bookingsUsecase struct {
	bookingsRepository Repository
	contextTimeout     time.Duration
}

func NewBookingsUsecase(timeout time.Duration, cr Repository) Usecase {
	return &bookingsUsecase{
		contextTimeout:     timeout,
		bookingsRepository: cr,
	}
}

func (uc *bookingsUsecase) GetByID(ctx context.Context, ID int) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if ID <= 0 {
		return Domain{}, usecase.ErrNotFound
	}
	res, err := uc.bookingsRepository.GetByID(ctx, ID)
	if err != nil {
		return Domain{}, err
	}

	return res, nil
}

func (uc *bookingsUsecase) Store(ctx context.Context, bookingDomain *Domain) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()
	fmt.Println("Test UC")
	result, err := uc.bookingsRepository.Store(ctx, bookingDomain)
	if err != nil {
		return Domain{}, err
	}

	return result, nil
}
