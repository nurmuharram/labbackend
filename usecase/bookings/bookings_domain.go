package bookings

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Domain struct {
	ID          int
	LabID       int
	LabName     string
	Description string
	Name        string
	Date        string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

type Usecase interface {
	GetByID(ctx context.Context, ID int) (Domain, error)
	Store(ctx context.Context, bookingDomain *Domain) (Domain, error)
}

type Repository interface {
	GetByID(ctx context.Context, ID int) (Domain, error)
	Store(ctx context.Context, bookingDomain *Domain) (Domain, error)
}
