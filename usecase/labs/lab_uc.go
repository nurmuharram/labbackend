package labs

import (
	"context"
	usecase "idsbackend/usecase"
	"time"
)

type labUsecase struct {
	labRepository  Repository
	contextTimeout time.Duration
}

func NewLabUsecase(timeout time.Duration, cr Repository) Usecase {
	return &labUsecase{
		contextTimeout: timeout,
		labRepository:  cr,
	}
}

func (uc *labUsecase) FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if page <= 0 {
		page = 1
	}
	if perpage <= 0 {
		perpage = 25
	}

	res, total, err := uc.labRepository.FindAll(ctx, page, perpage)
	if err != nil {
		return []Domain{}, 0, err
	}

	return res, total, nil
}

func (uc *labUsecase) GetAll(ctx context.Context) ([]Domain, error) {
	resp, err := uc.labRepository.Find(ctx)
	if err != nil {
		return []Domain{}, err
	}
	return resp, nil
}

func (uc *labUsecase) GetByID(ctx context.Context, labID int) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if labID <= 0 {
		return Domain{}, usecase.ErrLabNotFound
	}
	res, err := uc.labRepository.GetByID(ctx, labID)
	if err != nil {
		return Domain{}, err
	}

	return res, nil
}

func (uc *labUsecase) Store(ctx context.Context, labDomain *Domain) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	result, err := uc.labRepository.Store(ctx, labDomain)
	if err != nil {
		return Domain{}, err
	}

	return result, nil
}
