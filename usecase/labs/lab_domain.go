package labs

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Domain struct {
	ID          int
	LabName     string
	Price       float32
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

type Usecase interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	GetAll(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, labId int) (Domain, error)
	Store(ctx context.Context, labDomain *Domain) (Domain, error)
}

type Repository interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	Find(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, labId int) (Domain, error)
	Store(ctx context.Context, labsDomain *Domain) (Domain, error)
}
