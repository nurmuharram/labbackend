package main

import (
	_routes "idsbackend/app/routes"

	_bookingsController "idsbackend/controllers/bookings"
	_labsController "idsbackend/controllers/labs"
	_bookingsRepo "idsbackend/repository/databases/bookings"
	_labsRepo "idsbackend/repository/databases/labs"
	_bookingsUsecase "idsbackend/usecase/bookings"
	_labsUsecase "idsbackend/usecase/labs"

	_dbDriver "idsbackend/repository/mysql"

	"log"
	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		log.Println("Service RUN on DEBUG mode")
	}
}

func main() {
	configDB := _dbDriver.ConfigDB{
		DB_Username: viper.GetString(`database.user`),
		DB_Password: viper.GetString(`database.pass`),
		DB_Host:     viper.GetString(`database.host`),
		DB_Port:     viper.GetString(`database.port`),
		DB_Database: viper.GetString(`database.name`),
	}
	db := configDB.InitDB()

	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second

	e := echo.New()

	// Labs ...
	labsRepo := _labsRepo.NewMySQLLabRepository(db)
	labsUsecase := _labsUsecase.NewLabUsecase(timeoutContext, labsRepo)
	labsCtrl := _labsController.NewLabController(labsUsecase)

	// bookings ...
	bookingsRepo := _bookingsRepo.NewMySQLBookingsRepository(db)
	bookingsUsecase := _bookingsUsecase.NewBookingsUsecase(timeoutContext, bookingsRepo)
	bookingsCtrl := _bookingsController.NewBookingsController(bookingsUsecase)

	routesInit := _routes.ControllerList{
		BookingsController: *bookingsCtrl,
		LabsController:     *labsCtrl,
	}
	routesInit.RouteRegister(e)

	log.Fatal(e.Start(viper.GetString("server.address")))
}
