package routes

import (
	"idsbackend/controllers/bookings"
	"idsbackend/controllers/labs"

	echo "github.com/labstack/echo/v4"
)

type ControllerList struct {
	BookingsController bookings.BookingsController
	LabsController     labs.LabController
}

func (cl *ControllerList) RouteRegister(e *echo.Echo) {
	r := e.Group("/api")

	//Labs ...
	lab := r.Group("/labs")

	lab.GET("/select", cl.LabsController.SelectAll)
	lab.GET("", cl.LabsController.FindAll)
	lab.GET("/id/:id", cl.LabsController.FindById)
	lab.POST("", cl.LabsController.Store)

	//Bookings ...
	booking := r.Group("/bookings")
	booking.GET("/id/:id", cl.BookingsController.GetById)
	booking.POST("", cl.BookingsController.Store)
}
