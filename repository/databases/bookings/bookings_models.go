package bookings

import (
	"idsbackend/repository/databases/labs"
	"idsbackend/usecase/bookings"

	"time"

	"gorm.io/gorm"
)

type Bookings struct {
	ID        int
	LabID     int
	Labs      labs.Labs `gorm:"foreignKey:LabID;references:ID"`
	Name      string
	Date      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}

func (rec *Bookings) toDomain() bookings.Domain {
	return bookings.Domain{
		ID:        rec.ID,
		LabID:     rec.LabID,
		Name:      rec.Name,
		Date:      rec.Date,
		CreatedAt: rec.CreatedAt,
		UpdatedAt: rec.UpdatedAt,
		DeletedAt: rec.DeletedAt,
	}
}

func fromDomain(bookingsDomain bookings.Domain) *Bookings {
	return &Bookings{
		ID:        bookingsDomain.ID,
		LabID:     bookingsDomain.LabID,
		Name:      bookingsDomain.Name,
		Date:      bookingsDomain.Date,
		CreatedAt: bookingsDomain.CreatedAt,
		UpdatedAt: bookingsDomain.UpdatedAt,
		DeletedAt: bookingsDomain.DeletedAt,
	}
}
