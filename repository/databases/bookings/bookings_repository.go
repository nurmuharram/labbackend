package bookings

import (
	"context"
	"fmt"
	"idsbackend/usecase/bookings"

	"gorm.io/gorm"
)

type mysqlBookingsRepository struct {
	DB *gorm.DB
}

func NewMySQLBookingsRepository(conn *gorm.DB) bookings.Repository {
	return &mysqlBookingsRepository{
		DB: conn,
	}
}

func (repo *mysqlBookingsRepository) GetByID(ctx context.Context, ID int) (bookings.Domain, error) {
	rec := Bookings{}
	err := repo.DB.Joins("Labs").Where("bookings.id = ?", ID).First(&rec).Error
	if err != nil {
		return bookings.Domain{}, err
	}
	return rec.toDomain(), nil
}

func (repo *mysqlBookingsRepository) Store(ctx context.Context, bookingsDomain *bookings.Domain) (bookings.Domain, error) {
	fmt.Println("Test Repo")
	rec := fromDomain(*bookingsDomain)

	result := repo.DB.Create(&rec)
	if result.Error != nil {
		return bookings.Domain{}, result.Error
	}

	return rec.toDomain(), nil
}

func (repo *mysqlBookingsRepository) Delete(ctx context.Context, bookingsDomain *bookings.Domain) (bookings.Domain, error) {
	rec := fromDomain(*bookingsDomain)

	result := repo.DB.Delete(rec)

	if result.Error != nil {
		return bookings.Domain{}, result.Error
	}

	return rec.toDomain(), nil
}
