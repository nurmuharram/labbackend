package labs

import (
	"idsbackend/usecase/labs"

	"time"

	"gorm.io/gorm"
)

type Labs struct {
	ID          int
	LabName     string
	Price       float32
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

func fromDomain(labsDomain labs.Domain) *Labs {
	return &Labs{
		ID:          labsDomain.ID,
		LabName:     labsDomain.LabName,
		Price:       labsDomain.Price,
		Description: labsDomain.Description,
		CreatedAt:   labsDomain.CreatedAt,
		UpdatedAt:   labsDomain.UpdatedAt,
		DeletedAt:   labsDomain.DeletedAt,
	}
}

func (rec *Labs) toDomain() labs.Domain {
	return labs.Domain{
		ID:          rec.ID,
		LabName:     rec.LabName,
		Price:       rec.Price,
		Description: rec.Description,
		CreatedAt:   rec.CreatedAt,
		UpdatedAt:   rec.UpdatedAt,
		DeletedAt:   rec.DeletedAt,
	}
}
