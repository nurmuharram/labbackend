package labs

import (
	"context"
	"idsbackend/usecase/labs"

	"gorm.io/gorm"
)

type mysqlLabsRepository struct {
	DB *gorm.DB
}

func NewMySQLLabRepository(conn *gorm.DB) labs.Repository {
	return &mysqlLabsRepository{
		DB: conn,
	}
}

func (repo *mysqlLabsRepository) FindAll(ctx context.Context, page, perpage int) ([]labs.Domain, int, error) {
	rec := []Labs{}

	offset := (page - 1) * perpage
	err := repo.DB.Find(&rec).Offset(offset).Limit(perpage).Error
	if err != nil {
		return []labs.Domain{}, 0, err
	}

	var totalData int64
	err = repo.DB.Model(&rec).Count(&totalData).Error
	if err != nil {
		return []labs.Domain{}, 0, err
	}

	var domainLab []labs.Domain
	for _, value := range rec {
		domainLab = append(domainLab, value.toDomain())
	}
	return domainLab, int(totalData), nil
}

func (repo *mysqlLabsRepository) Find(ctx context.Context) ([]labs.Domain, error) {
	rec := []Labs{}

	repo.DB.Find(&rec)
	labDomain := []labs.Domain{}
	for _, value := range rec {
		labDomain = append(labDomain, value.toDomain())
	}

	return labDomain, nil
}

func (repo *mysqlLabsRepository) GetByID(ctx context.Context, labId int) (labs.Domain, error) {
	rec := Labs{}
	err := repo.DB.Where("id = ?", labId).First(&rec).Error
	if err != nil {
		return labs.Domain{}, err
	}
	return rec.toDomain(), nil
}

// func (repo *mysqlLabsRepository) GetByName(ctx context.Context, categoryName string) (labs.Domain, error) {
// 	rec := Labs{}
// 	err := repo.DB.Where("name = ?", categoryName).First(&rec).Error
// 	if err != nil {
// 		return labs.Domain{}, err
// 	}
// 	return rec.toDomain(), nil
// }

func (repo *mysqlLabsRepository) Store(ctx context.Context, labsDomain *labs.Domain) (labs.Domain, error) {
	rec := fromDomain(*labsDomain)

	result := repo.DB.Create(&rec)
	if result.Error != nil {
		return labs.Domain{}, result.Error
	}

	return rec.toDomain(), nil
}
